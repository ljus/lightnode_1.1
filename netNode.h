/************** CONSTANT PARAMETERS *************/
/* ARTNET CODES */
#define ARTNET_DATA 0x50
#define ARTNET_POLL 0x20
#define ARTNET_POLL_REPLY 0x21
#define ARTNET_HEADER_SIZE    18
/* sACN CODES */
#define VECTOR_ROOT_E131_DATA                 0x00000004  // root layer byte index 18-21
#define VECTOR_ROOT_E131_EXTENDED             0x00000008  // root layer byte index 18-21
#define VECTOR_E131_DATA_PACKET               0x00000002  // framing layer byte index 2-5
#define VECTOR_E131_EXTENDED_SYNCHRONIZATION  0x00000001  // framing layer byte index 2-5
#define VECTOR_E131_EXTENDED_DISCOVERY        0x00000002  // framing layer byte index 2-5
#define VECTOR_DMP_SET_PROPERTY               0x02        // DMP layer byte index 2
#define SACN_ROOT_SIZE        38
#define SACN_FRAMING_SIZE     77
#define SACN_DMP_HEADER_SIZE  11

/************** FUNCTION PROTOTYPES *************/
bool sACN();
bool artNet();

/* Latest DMX data */
uint8_t prevPackIndex[2] = {0,0};
uint16_t uniSize;     // Size of current universe
uint8_t dmxData[2][514];
int packSize = 0;
WiFiUDP udp_artnet;
WiFiUDP udp_sacn;

bool setup_netNode() {}

bool netNode() {
  if (udp_artnet.parsePacket()) {
    artNet();
    udp_artnet.flush();
  } 
packSize = udp_sacn.parsePacket();
  if (packSize) {
    sACN();
    udp_sacn.flush();
  }
}

/* Parse sACN data */
bool sACN() {
  uint16_t uniLocalAddr;
  uint8_t sACN_rootHeader[SACN_ROOT_SIZE];
  uint8_t sACN_framingHeader[SACN_FRAMING_SIZE];
  uint8_t sACN_dmpHeader[SACN_DMP_HEADER_SIZE];
  
  udp_sacn.read(sACN_rootHeader, SACN_ROOT_SIZE);

  if (sACN_rootHeader[9] != '1' || sACN_rootHeader[10] != '.' || sACN_rootHeader[11] != '1' || sACN_rootHeader[12] != '7' || sACN_rootHeader[21] != VECTOR_ROOT_E131_DATA)
    return false;                                                                                                   //* Abort if NOT sacn dmx data
  
  udp_sacn.read(sACN_framingHeader, SACN_FRAMING_SIZE);
  uniLocalAddr = ((sACN_framingHeader[75] << 8) + sACN_framingHeader[76]) - WiFi.localIP()[3];

  /* Abort if icorrect univere */
  if (uniLocalAddr != 0 && uniLocalAddr != 1)
    return false;
  /* Ignore out of order packets */
  if (sACN_framingHeader[73] - prevPackIndex[uniLocalAddr] < 0 && sACN_framingHeader[73] - prevPackIndex[uniLocalAddr] > -200)
    return false;
  prevPackIndex[uniLocalAddr] = sACN_framingHeader[73]; // Set previous packet index = current packet index

  udp_sacn.read(sACN_dmpHeader, SACN_DMP_HEADER_SIZE);
  outputBit = (uniLocalAddr) ? (1<<OUTPUT2_PIN) : (1<<OUTPUT1_PIN);
  uniSize = (sACN_dmpHeader[8] << 8) + (sACN_dmpHeader[9]) - 1;

  if (SACN_ROOT_SIZE + SACN_FRAMING_SIZE + SACN_DMP_HEADER_SIZE + uniSize != packSize)
    return false; 

  udp_sacn.read(dmxData[uniLocalAddr], uniSize);
  if (uniSize > 4) {
    pixelNode(dmxData[uniLocalAddr], uniSize);
  } else if (uniSize > 3) {
    pixelNode4(dmxData[uniLocalAddr], uniSize);
  } else {
    pixelNode3(dmxData[uniLocalAddr], uniSize);
  }
Serial.println(uniSize);
  return true;
}

/* Parse Art-Net data */
bool artNet() {
  uint16_t uniLocalAddr;
  uint8_t header[ARTNET_HEADER_SIZE];

  /* Abort if NOT Art-Net DMX data*/
  udp_artnet.read(header, ARTNET_HEADER_SIZE);
  if (header[0] != 'A' || header[1] != 'r' || header[2] != 't' || header[8] != 0x00 || header[9] != ARTNET_DATA) 
    return false;
  
  uniLocalAddr = ((header[15] << 8) + header[14]) - WiFi.localIP()[3];
  
  /* Abort if icorrect univere */
  if (uniLocalAddr != 0 && uniLocalAddr != 1)
    return false;
  
  /* Ignore out of order packets */
  if (header[11] - prevPackIndex[uniLocalAddr] < 0 && header[11] - prevPackIndex[uniLocalAddr] > -200)
    return false;
  prevPackIndex[uniLocalAddr] = header[11]; // Set previous packet index = current packet index
  
  outputBit = (uniLocalAddr) ? (1<<OUTPUT2_PIN) : (1<<OUTPUT1_PIN);
  uniSize = (header[16] << 8) + (header[17]);  
  udp_artnet.read(dmxData[uniLocalAddr], uniSize);
  pixelNode(dmxData[uniLocalAddr], uniSize);
  return true;
}
