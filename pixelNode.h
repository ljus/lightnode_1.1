/************** CONSTANT PARAMETERS *************/
#ifdef ESP32
  #define GPIO_SET GPIO_OUT_W1TS_REG
  #define GPIO_CLEAR GPIO_OUT_W1TC_REG
  #define OUTPUT1_PIN 22 // Pin D1
  #define OUTPUT2_PIN 21 // Pin D2
  #define OUTPUT3_PIN 17 // Pin D3
  #define OUTPUT4_PIN 16 // Pin D4
  uint32_t outputBit = 0b00000000011000110000000000000000;
#elif ESP8266
  #define GPIO_SET PERIPHS_GPIO_BASEADDR + GPIO_OUT_W1TS_ADDRESS
  #define GPIO_CLEAR PERIPHS_GPIO_BASEADDR + GPIO_OUT_W1TC_ADDRESS
  #define OUTPUT1_PIN 5 // Pin D1
  #define OUTPUT2_PIN 4 // Pin D2
  #define OUTPUT3_PIN 2 // Pin D3
  #define OUTPUT4_PIN 0 // Pin D4
  uint16_t outputBit = 0b0000000000110101;
#endif
//  outputBit = (1<<OUTPUT1_PIN) | (1<<OUTPUT2_PIN) | (1<<OUTPUT3_PIN) | (1<<OUTPUT4_PIN);

/************** FUNCTION PROTOTYPES *************/
void bitbanger(uint8_t);
void rgbbanger(uint32_t);
  
bool setup_pixelNode() {
  pinMode(OUTPUT1_PIN, OUTPUT);
  pinMode(OUTPUT2_PIN, OUTPUT);
  pinMode(OUTPUT3_PIN, OUTPUT);
  pinMode(OUTPUT4_PIN, OUTPUT);
}

bool ICACHE_FLASH_ATTR pixelNode3(uint8_t *uniData, uint16_t &uniSize) {
  uint16_t chan;
  ets_intr_lock();
  for (uint16_t n = 0; n < 60; n++) {  // outer loop counting bytes
    for (uint16_t t = 0; t < 3; t++) {  // outer loop counting bytes
      bitbanger(uniData[t]); // ~10us
    }
  }
  ets_intr_unlock();
}
bool ICACHE_FLASH_ATTR pixelNode4(uint8_t *uniData, uint16_t &uniSize) {
  uint16_t chan;
  ets_intr_lock();
  for (uint16_t n = 0; n < 45; n++) {  // outer loop counting bytes
    for (uint16_t t = 0; t < 4; t++) {  // outer loop counting bytes
      bitbanger(uniData[t]); // ~10us
    }
  }
  ets_intr_unlock();
}
bool ICACHE_FLASH_ATTR pixelNode(uint8_t *uniData, uint16_t &uniSize) {
  uint16_t chan;
  ets_intr_lock();
  for (uint16_t t = 0; t < uniSize; t++) {  // outer loop counting bytes
    bitbanger(uniData[t]); // ~10us
  }
  ets_intr_unlock();
}

void ICACHE_RAM_ATTR bitbanger(uint8_t chData) {
  uint8_t n;
  uint8_t bitMask = 0x80;
  while (bitMask) {
    // T = 0ns
    n = 4;
    while (n--) WRITE_PERI_REG( GPIO_SET, outputBit );  // 75ns @ 160MHz
    // T = 300ns @ 160MHz
    n = 4;
    if (chData & bitMask)
      while (n--) WRITE_PERI_REG( GPIO_CLEAR, 0 );  // 75ns @ 160MHz
    else
      while (n--) WRITE_PERI_REG( GPIO_CLEAR, outputBit );  // 75ns @ 160MHz
    // T = 900ns @ 160MHz
    n = 8;
    while (n--) WRITE_PERI_REG( GPIO_CLEAR, outputBit );  // 75ns @ 160MHz
    // T = 1200ns @ 160MHz
    bitMask >>= 1;
  }
}

void ICACHE_RAM_ATTR rgbbanger(uint32_t data) {
  bitbanger((data >> 8) & 0xFF);
  bitbanger((data >> 16) & 0xFF);
  bitbanger(data & 0xFF);
//  bitbanger(0x00);
}
